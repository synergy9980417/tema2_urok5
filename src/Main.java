
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
//
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;

//import static com.codeborne.selenide.Selenide.*;


//import static com.codeborne.selenide.Selenide.SeleniumDriver;
//import static com.codeborne.selenide.Condition.*;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeOptions;

public class Main {
    public static void main(String[] args) throws IOException {

//        Урок 5. Введение в массивы
//        Цель задания:
//        Знакомство с таким типом данных, как массивы, получение базовых навыков
//        работы с массивами
//        Задание:
//        1.
//        Пользователь вводит 10 чисел, сохраните их в массив



    Scanner scanner = new Scanner(System.in);
    int[] in1 = new int[10];

    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
    for (int i=0;i<10;i++){
    System.out.println("введите число номер "+(i+1)+" :");
    in1[i]=scanner.nextInt();

}

//        2.
//        Сохраните 10 чисел в массив, выведите их с конца

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("выводим массив наоборот");
        for (int i=9;i>=0;i--){
            System.out.println(in1[i]);
        }
//        3.
//        Сохраните 10 строк в массив, выведите с конца каждую вторую

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        String[] str3 = new String[10];
        for (int i=0;i<=str3.length-1;i++) {
            System.out.println("ведите строку "+(i+1)+" из "+str3.length+" :");
            str3[i]=scanner.next();

        }

        for (int i=str3.length-1;i>=0;i--){
            if (((i+1)%2==0)||(i==0))
            System.out.println("string ("+(i+1)+"):"+str3[i]);
        }
//        4.
//        В файле 10 дробных чисел. Считайте в массив, выведите те, что больше
//        числа пи.


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

                InputStream stream = new FileInputStream("file.txt");
                Scanner filescan = new Scanner(stream);
                float[] f4=new float[10];
                for (int i=0; i<10;i++) {
                    f4[i] = (filescan.nextFloat());
                }


        for (int i=0; i<10;i++) {
            if (f4[i]>Math.PI) System.out.println(f4[i]);
        }

//        5.
//        Пользователь вводит буквы. Пока не введёт букву ‘ю’, сохраняйте буквы в
//        массив char(пусть в нем будет 100 элементов
//        максимум). Потом создайте
//        новый массив char размером столько, сколько букв ввел пользователь.
//                Скопируйте в него буквы из первого массива

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        char[] ch5_1 = new char[100];
        byte i5 = -1 ;
        do
        {
            i5++;
            if (i5==99)break;
            ch5_1[i5]=scanner.next().charAt(0);
        }while (ch5_1[i5]!='ю');
       //так как i5 с 0 то количество элементов +1
        char[] ch5_2 = new char[i5+1];
        for (int i=0; i<ch5_2.length;i++){
            ch5_2[i]=ch5_1[i];
        }
        System.out.println("скопировано "+ch5_2.length);
//        6.
//        Есть 2 файла по 5 строк в каждом. Сохраните их в 2 массива. Создайте
//        третий массив из 10 строк, скопируйте в него стро
//        ки из первых двух
//        массивов.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        InputStream stream1 = new FileInputStream("file1.txt");
        InputStream stream2 = new FileInputStream("file2.txt");
        Scanner filescan1 = new Scanner(stream1);
        Scanner filescan2 = new Scanner(stream2);


        String[] f6_1=new String[5];
        for (int i=0; i<5;i++) {
            f6_1[i] = (filescan1.nextLine());
        }

        String[] f6_2=new String[5];
        for (int i=0; i<5;i++) {
            f6_2[i] = (filescan2.nextLine());
        }


        String[] f6_3=new String[10];

       for (int i=0;i<10;i++) {
           if (i<5){f6_3[i]=f6_1[i];} else
           {f6_3[i]=f6_3[i-5];}
       }


//        7.
//        Поле чудес. Игрок один пишет слово. Выводится: ##### (по количеству
//        букв) Игрок два пытается угадать буквы. Если угадал, буква открывается:
//#а#а#, ба#а#, ... банан

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Игрок вводит слово:");
        Scanner scanner7 = new Scanner(System.in);
        String word7 = scanner7.nextLine();
       //массив отгаданных букв
        char[] wstr7 = new char[word7.length()];
        for (int i=0;i<300;i++) System.out.println("");

        System.out.println("Другой игрок, отгадывает введённое слово:");
        for (int i=0;i<word7.length();i++) {
            wstr7[i]='#';
        }

        //количество отгаданных инкрементируется в well7
        byte well7=0;
        char ch7;

        while (well7<word7.length()) {
            System.out.println("состояние отгдавываемого слова на текущий момент:");
            for (int i=0;i<word7.length();i++) System.out.print(wstr7[i]);
            System.out.println("");
            System.out.print("Введите букву: ");
            ch7=scanner7.next().charAt(0);
            System.out.print("\n");
            for (int i=0;i<word7.length();i++){
                if (word7.charAt(i)==ch7 && wstr7[i]=='#'){
                    wstr7[i]=ch7;
                    well7++;
                }
            }
        }
        System.out.println("Слово было угадано");
        for (int i=0;i<word7.length();i++) System.out.print(wstr7[i]);
        System.out.println("");



//        8.
//        Поле чудес с соревнованием. Генерируете 1000 случайных слов на сай
//        те


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");

//                -
//                генераторе (пример:
//        https://sanstv.ru/randomWord
//, но в поисковике есть
//        много подобных). Сохраняете их в файл. Из файла считываете случайное, и
//        выводите ####... игроки по очереди отгадывают букву. Гласная
//                -
//                1
//        балл,
//                согласная
//                        -
//                        2 балла, если буква встречается несколько раз
//        -
//                больше. Можно
//        угадать слово целиком
//                -
//                за каждую угаданную букву тогда начислять
//        баллы(гласные 1, согласные 2). Если не угадал
//        -
//                минус 3 балла.

        System.setProperty("webdriver.chrome.driver", "chromedriver-linux64/chromedriver");

        // Создайте экземпляр ChromeOptions и добавьте параметр headless
        //чтобы не отображать работу браузера chrome
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--headless");

        // Создайте экземпляр ChromeDriver
        WebDriver driver = new ChromeDriver(options);
        String fileName ="file8.txt";
        int numStr;
        //создаём писателя в файл
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

        try {
            // Откройте веб-страницу
            driver.get("https://sanstv.ru/randomWord");

            // Найдите все элементы с классом strong2
            List<WebElement> elements = driver.findElements(By.cssSelector("a.strong2"));
            //каждый раз перед записью отчищаем файл
            clearFile(fileName);
            //инициируем счётчик строк в 0
            numStr = 0;
            // Выведите текст каждого элемента
            for (WebElement element : elements) {
                String str = new String();
                str=element.getText();
             //   System.out.println(str);
                // запишем строку в файл
                writer.write(str+"\n");
                //посчитаем строку
                numStr++;
            }
        } finally {
            // Закройте браузер
            driver.quit();
            // закроем writer
            writer.close();
        }
        //создаём рандомайзер для получения номера строки которую скачаем
        Random num = new Random(numStr);

        //скачиваем случайную строку из файла
        String str8 = getStringFromFile(fileName,num.nextInt(numStr));

        System.out.println("Слово: "+str8);

        char[] openChar = new char[str8.length()];
        System.out.println("игроки, отгадывают слово:");
        for (int i = 0; i < str8.length(); i++) {
            openChar[i] = '#';
        }
        //массив игроков. по заданию их 2
        Player players[] = new Player[2];
        //инициируем объекты
        players[0]=new Player(1,0);
        players[1]=new Player(2,0);
        //общий счётчик отгаданных в слове букв
        int well = 0;
       //входим цикл пока количество отгаданных букв не будет равно длине строки
            while (well < str8.length()) {
                System.out.println("состояние отгдавываемого слова на текущий момент:");
                for (int i = 0; i < str8.length(); i++) System.out.print(openChar[i]);
                System.out.println("");

                for (int j = 0; j < 2; j++) {
                    //в этом методе запрашиваем букву или слово целиком у игрока
                    System.out.println("Игрок номер "+players[j].getNumber()+": ");
                    String openStr = players[j].charWord();

                    //в любой момент игрок может ввести слово целиком. поэтому проверяем
                    if (str8.length()==openStr.length() && str8.equals(openStr)) {
                        well=str8.length();
                        System.out.println("Правильно это слово: "+str8);
                        //весь массив символов приводив в соответствие с угаданным словом
                        for (int k=0;k<openStr.length();k++){
                            openChar[k]=openStr.charAt(k);
                        }
                    } else {
                        int well_=well;
                        for (int i = 0; i < str8.length(); i++) {
                            if (str8.charAt(i) == openStr.charAt(0) && openChar[i] == '#') {
                                players[j].setScore((players[j].getScore()+Player.thisScore(openStr.charAt(0))));
                                openChar[i] = openStr.charAt(0);
                                System.out.println("УРА! есть такая буква");
                                well++;
                            }
                        }
                        if (well_==well) System.out.println("НЕТ! такой буквы");
                    }
                    System.out.println("!!!!!!!!!!!! состояние игрока после хода !!!!!!!!!!!");
                    players[j].printPlayer();
                    System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    if (well==str8.length()){
                        System.out.println("Игрок "+players[j].getNumber()+" выиграл т.к. угадал слово");
                        for (int i = 0; i < str8.length(); i++) System.out.print(openChar[i]);
                        System.out.println("");
                        if (players[0].score>players[1].score){
                            System.out.println("Первый игрок имеет больше очков по угаданным буквам");
                        } else
                        if (players[1].score>players[0].score){
                            System.out.println("Второй игрок имеет больше очков по угаданным буквам");
                        } else
                            System.out.println("игроки набрали одинаковое количество очков");
                        break;
                    }

                }
            }
        }


    public static class Player {
        static public int thisScore(char ch){
            char[] glasn = new char[] {'ы','а','о','э','й','у','е','ъ','я','и','ь','ю'};
            char[] soglasn = new char[] {'ц','к','н','г','ш','щ','з','х','ф','в','п','р','л','д','ж','ч','с','м','т','б'};

            int score=0;
            for (int i = 0; i < glasn.length; i++) {
                if (ch==glasn[i])
                    score+=1;
                else
                if (ch==soglasn[i])
                    score+=2;
            }
            return score;
        }


        //номер игрока
        private int number;
        //его очки
        private int score;

        public int setNumber(int number) {
            this.number = number;
            return number;
        }

        public int setScore(int score) {
            this.score = score;
            return score;
        }

        public int getNumber() {
            return number;
        }

        public int getScore() {
            return score;
        }

        public Player(int number, int score) {
            //массив отгаданных букв
            this.number=number;
            this.score = score;
        }


        public void printPlayer() {
            System.out.println("Игрок номер: " + this.getNumber());
            System.out.println("Его количество набранных очков: " + this.getScore());
        }

        public String charWord() {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Введите букву: ");
            return scanner.next();//.charAt(0);
            // System.out.print("\n");
        }
    }


    private static String downloadWebPage(String url) throws IOException {
        StringBuilder result = new StringBuilder();
        String line;
        URLConnection urlConnection = new URL(url).openConnection();
        try (InputStream is = urlConnection.getInputStream();
             BufferedReader br= new BufferedReader(new InputStreamReader(is))){
            while ((line=br.readLine())!=null){
                result.append(line);
            }
        }
        return result.toString();

    }

    //считываем из файла строку под номером numStr
    public static String getStringFromFile(String file, int numStr) throws FileNotFoundException {

        StringBuilder sb = new StringBuilder();
        String result ="";

        InputStream stream = new FileInputStream(file);

        Scanner filescan = new Scanner(stream);

        for (int i=1; i<=numStr;i++) {
            result=(filescan.nextLine());
        }
        return result;
    }

//запись в файл строку str
    public static void writeToFile(String str, String fileName) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
        writer.write(str);
        writer.close();
    }
    //отчистка файла
public static void clearFile(String file) throws FileNotFoundException {
    PrintWriter writerClear = new PrintWriter(file);
    writerClear.print("");
    writerClear.close();
}



}